package simulation.models;

import lombok.Getter;
import lombok.Setter;
import simulation.enums.Action;
import simulation.enums.Direction;

import java.util.Optional;
import java.util.Queue;

public class Hunter
{
    @Getter
    private String name;
    @Getter
    @Setter
    private Direction side;
    @Getter
    private Position position;
    private Queue<Action> actions;
    @Getter
    private int treasures;
    private boolean canCollect;


    public Hunter(String name, Position position, Direction side, Queue<Action> actions) {
        this.name = name;
        this.side = side;
        this.position = position;
        this.actions = actions;
        this.treasures = 0;
        this.canCollect = true;
    }

    public boolean occupe(Position position) {
        return this.position.equals(position);
    }

    public boolean canCollect() {
        return this.canCollect;
    }

    public void move(Position next) {
        this.position = next;
        this.canCollect = true;
    }

    public void collectTreasure() {
        if (!this.canCollect) return;

        this.canCollect = false;
        this.treasures++;
    }

    public boolean idle() {
        return this.actions.isEmpty();
    }

    public boolean canActMore() {
        return !idle();
    }

    public Optional<Action> nextAction() {
        return Optional.ofNullable(this.actions.poll());
    }
}
