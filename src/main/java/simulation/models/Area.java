package simulation.models;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.HashMap;
import java.util.Map;

@Log4j2
@Getter
public class Area
{
    private int width;
    private int height;
    private Map<Position, Square> squares;

    public Area(int width, int height) {
        this.width = width;
        this.height = height;
        this.squares = new HashMap<>();
    }

    public boolean isOutside(Position position) {
        if (position == null) return true;

        int x = position.getX(), y = position.getY();
        return x < 0 || x >= width || y < 0 || y >= height;
    }

    public boolean isMountain(Position position) {
        if (isOutside(position)) return false;
        return squares.get(position) instanceof Mountain;
    }

    public void addMountain(Position position) {
        if (isOutside(position)) return;
        squares.put(position, new Mountain());
    }

    public void addTreasure(Position position, int quantity) {
        if (isOutside(position)) return;
        squares.put(position, new Treasure(quantity));
    }

    public boolean collectTreasure(Position position) {
        if (isMountain(position)) return false;

        Treasure treasure = (Treasure) squares.get(position);
        if (treasure == null) {
            log.warn(String.format(
                    "No treasures found @ (%d,%d)",
                    position.getX(), position.getY()
            ));
            return false;
        }

        boolean collected = treasure.collect();
        log.warn(collected
                ? String.format("%d left treasures @ (%d,%d)", treasure.getQuantity(), position.getX(), position.getY())
                : String.format("No more treasures @ (%d,%d)", position.getX(), position.getY())
        );
        return collected;
    }
}
