package simulation.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;

@Getter
@AllArgsConstructor
public class Position
{
    private int x;
    private int y;

    @Override
    public boolean equals(Object object) {
        if (object == null || this.getClass() != object.getClass()) return false;

        Position position = (Position) object;
        return x == position.x && y == position.y;
    }

    @Override
    public int hashCode() {
        return ( 17 * x ) + y;
    }

    public static Position add(Position first, Position second) {
        Position a = Optional.ofNullable(first).orElse(new Position(0,0));
        Position b = Optional.ofNullable(second).orElse(new Position(0,0));

        return new Position(a.x + b.x, a.y + b.y);
    }
}
