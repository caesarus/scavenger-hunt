package simulation.models;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class Treasure extends Square
{
    private int quantity = 0;

    public Treasure(int quantity) {
        this.quantity = Math.max(0, quantity);
    }

    public boolean collect() {
        if (quantity == 0) return false;

        quantity--;
        return true;
    }
}
