package simulation.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class Simulation
{
    private Area area;
    private List<Hunter> hunters;
}
