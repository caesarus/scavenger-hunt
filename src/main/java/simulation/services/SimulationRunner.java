package simulation.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import simulation.actions.*;
import simulation.models.Hunter;
import simulation.models.Position;
import simulation.models.Simulation;

import java.util.*;

import static simulation.enums.Action.TURN_LEFT;
import static simulation.enums.Action.TURN_RIGHT;

@Log4j2
@Service
public class SimulationRunner
{
    private boolean resume = true;

    public void run(Simulation simulation) {
        log.info("Here we go !");
        while (resume) {
            resume = false;
            this.iterate(simulation);
        }
    }

    void iterate(Simulation simulation) {
        List<Hunter> hunters = new ArrayList<>(simulation.getHunters());

        for (Hunter hunter : hunters) {
            if (hunter.idle()) {
                Position position = hunter.getPosition();
                log.info(String.format("'%s' idle @ (%d,%d)", hunter.getName(), position.getX(), position.getY()));
                continue;
            }

            resolveAction(hunter, simulation).ifPresent(action -> {
                action.run();
                new CollectTreasureAction(hunter, simulation.getArea()).run();
            });

            if (hunter.canActMore()) this.resume = true;
        }
    }

    Optional<HunterAction> resolveAction(Hunter hunter, Simulation simulation) {
        return hunter.nextAction().map(action ->
                action == TURN_RIGHT ? new TurnRightAction(hunter)
              : action == TURN_LEFT ? new TurnLeftAction(hunter)
              : new MoveForwardAction(hunter, simulation.getArea(), simulation.getHunters())
        );
    }
}
