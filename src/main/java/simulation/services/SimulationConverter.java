package simulation.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import simulation.core.ConverterActions;
import simulation.enums.Action;
import simulation.enums.Direction;
import simulation.models.*;

import javax.annotation.Resource;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Log4j2
@Service
public class SimulationConverter implements ConverterActions
{
    @Resource
    private SimulationFactory factory;

    // input

    static final String REGEX_AREA      = "C - (\\d+) - (\\d+)";
    static final String REGEX_MOUNTAIN  = "M - (\\d+) - (\\d+)";
    static final String REGEX_TREASURE  = "T - (\\d+) - (\\d+) - (\\d+)";
    static final String REGEX_HUNTER    = "A - (\\w+) - (\\d+) - (\\d+) - ([NSOE]) - ([AGD]*)";

    public Simulation deserialize(List<String> lines) {
        List<Area> areas = new ArrayList<>();
        List<Position> mountains = new ArrayList<>();
        List<SimpleEntry<Position, Integer>> treasures = new ArrayList<>();
        List<Hunter> hunters = new ArrayList<>();

        for (String line : lines) {
            if (match(line, REGEX_AREA, this::parseAreaLine, areas::add)) continue;
            if (match(line, REGEX_MOUNTAIN, this::parseMountainLine, mountains::add)) continue;
            if (match(line, REGEX_TREASURE, this::parseTreasureLine, treasures::add)) continue;
            match(line, REGEX_HUNTER, this::parseHunterLine, hunters::add);
        }

        return this.factory.buildSimulation(areas, mountains, treasures, hunters);
    }
    <T> boolean match(String line, String regex, Function<String[], T> parser, Consumer<T> collector) {
        Matcher matcher = Pattern.compile(regex).matcher(line);
        boolean matched = matcher.matches();

        if (matched) {
            int count = matcher.groupCount();
            String[] groups = new String[count];
            for (int i = 0; i < count; i += 1) {
                groups[i] = matcher.group(i + 1);
            }

            T item = parser.apply(groups);
            collector.accept(item);
        }

        return matched;
    }

    Area parseAreaLine(String[] groups) {
        int width = Integer.valueOf(groups[0]);
        int height = Integer.valueOf(groups[1]);
        return new Area(width, height);
    }
    Position parseMountainLine(String[] groups) {
        int x = Integer.valueOf(groups[0]);
        int y = Integer.valueOf(groups[1]);
        return new Position(x, y);
    }
    SimpleEntry<Position, Integer> parseTreasureLine(String[] groups) {
        int x = Integer.valueOf(groups[0]);
        int y = Integer.valueOf(groups[1]);
        int treasures = Integer.valueOf(groups[2]);
        return new SimpleEntry<>(new Position(x, y), treasures);
    }
    Hunter parseHunterLine(String[] groups) {
        String name = groups[0];
        int x = Integer.valueOf(groups[1]);
        int y = Integer.valueOf(groups[2]);
        Direction side = Direction.from(groups[3].charAt(0));
        Queue<Action> actions = Action.from(groups[4]);
        return new Hunter(name, new Position(x, y), side, actions);
    }

    // output

    private static final String OUTPUT_AREA     = "C - %d - %d";
    private static final String OUTPUT_MOUNTAIN = "M - %d - %d";
    private static final String OUTPUT_TREASURE = "T - %d - %d - %d";
    private static final String OUTPUT_HUNTER   = "A - %s - %d - %d - %s - %d";

    public List<String> serialize(Simulation simulation) {
        List<String> lines = new ArrayList<>();

        Area area = simulation.getArea();
        lines.add(String.format( OUTPUT_AREA, area.getWidth(), area.getHeight() ));

        area.getSquares().entrySet().stream()
                .filter(entry -> entry.getValue() instanceof Mountain)
                .map(Map.Entry::getKey)
                .forEach(position  -> lines.add(String.format( OUTPUT_MOUNTAIN, position.getX(), position.getY() )));

        area.getSquares().entrySet().stream()
                .filter(entry -> entry.getValue() instanceof Treasure)
                .map(entry  -> {
                    Position position = entry.getKey();
                    int treasures = ((Treasure) entry.getValue()).getQuantity();
                    return new Object[] {position.getX(), position.getY(), treasures};
                })
                .forEach(args -> lines.add(String.format( OUTPUT_TREASURE, args )));

        simulation.getHunters().stream()
            .map(hunter -> new Object[] {
                hunter.getName(), hunter.getPosition().getX(), hunter.getPosition().getY(),
                hunter.getSide().toLetter(), hunter.getTreasures()
            }).forEach(args -> lines.add(String.format( OUTPUT_HUNTER, args )));

        return lines;
    }
}
