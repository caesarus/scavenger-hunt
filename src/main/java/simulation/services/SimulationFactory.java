package simulation.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import simulation.exceptions.ExitException;
import simulation.models.Area;
import simulation.models.Hunter;
import simulation.models.Position;
import simulation.models.Simulation;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@Service
public class SimulationFactory
{
    public Simulation buildSimulation(List<Area> areas, List<Position> mountains,
                                      List<SimpleEntry<Position, Integer>> treasures,
                                      List<Hunter> hunters)
    {
        Area area = this.prepareArea(areas);
        this.prepareMountains(area, mountains);
        this.prepareTreasures(area, treasures);

        List<Hunter> men = this.prepareHunters(area, hunters);
        return new Simulation(area, men);
    }

    Area prepareArea(List<Area> areas) {
        if (areas.isEmpty()) {
            throw new ExitException("Missing area (ie: C - 3 - 4) inside the input file");
        }

        if (areas.size() > 1) {
            log.warn("More areas found in the input file: keep the first one");
        }

        return areas.get(0);
    }

    void prepareMountains(Area area, List<Position> mountains) {
        List<Position> placed = new ArrayList<>();
        List<Position> rejected = new ArrayList<>();

        for (Position position : mountains) {
            boolean invalid = area.isMountain(position) || placed.contains(position);

            if (invalid) rejected.add(position);
            else placed.add(position);
        }

        if (!rejected.isEmpty()) {
            String listing = rejected.stream()
                    .map(pos -> String.format("(%d,%d)", pos.getX(), pos.getY()))
                    .collect(Collectors.joining(", "));
            log.warn("Invalid mountains found: " + listing);
        }

        for (Position pos : placed) {
            area.addMountain(pos);
        }
    }

    void prepareTreasures(Area area, List<SimpleEntry<Position, Integer>> treasures) {
        Map<Position, Integer> quantities = new HashMap<>();
        List<Position> rejected = new ArrayList<>();

        for (SimpleEntry<Position, Integer> entry : treasures) {
            Position position = entry.getKey();
            boolean invalid = area.isMountain(position) || quantities.containsKey(position);

            if (invalid) rejected.add(position);
            else quantities.put(position, entry.getValue());
        }

        if (!rejected.isEmpty()) {
            String listing = rejected.stream()
                    .map(pos -> String.format("(%d,%d)", pos.getX(), pos.getY()))
                    .collect(Collectors.joining(", "));
            log.warn("Invalid treasures found: " + listing);
        }

        for (Map.Entry<Position, Integer> entry : quantities.entrySet()) {
            area.addTreasure(entry.getKey(), entry.getValue());
        }
    }

    List<Hunter> prepareHunters(Area area, List<Hunter> hunters) {
        List<Hunter> placed = new ArrayList<>();
        List<Hunter> rejected = new ArrayList<>();

        for (Hunter hunter : hunters) {
            Position position = hunter.getPosition();
            boolean invalid = area.isOutside(position)
                    || area.isMountain(position)
                    || placed.stream().anyMatch(man -> man.occupe(position));

            if (invalid) rejected.add(hunter);
            else placed.add(hunter);
        }

        if (!rejected.isEmpty()) {
            String listing = rejected.stream()
                    .map(hunter -> String.format(
                            "'%s' @ (%d,%d)", hunter.getName(),
                            hunter.getPosition().getX(), hunter.getPosition().getY()
                    ))
                    .collect(Collectors.joining(", "));
            log.warn("Invalid hunters found: " + listing);
        }

        return placed;
    }
}
