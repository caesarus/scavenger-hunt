package simulation.enums;

public enum Direction
{
    NORTH('N'),
    SOUTH('S'),
    WEST('O'),
    EAST('E');

    private char letter;

    public char toLetter() {
        return this.letter;
    }

    Direction(char letter) {
        this.letter = letter;
    }

    public static Direction from(char letter) {
        return 'N' == letter ? NORTH
             : 'S' == letter ? SOUTH
             : 'O' == letter ? WEST
             :  EAST;
    }
}
