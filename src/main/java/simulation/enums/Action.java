package simulation.enums;

import java.util.LinkedList;
import java.util.Queue;

public enum Action
{
    MOVE_FORWARD('A'),
    TURN_LEFT('G'),
    TURN_RIGHT('D');
    private char letter;

    Action(char letter) {
        this.letter = letter;
    }

    public static Queue<Action> from(String sequence) {
        Queue<Action> actions = new LinkedList<>();
        if (sequence == null) return actions;

        sequence.chars()
            .mapToObj(in -> (char) in)
            .map(Action::from)
            .forEach(actions::add);

        return actions;
    }
    private static Action from(char letter) {
        return 'D' == letter ? TURN_RIGHT
             : 'G' == letter ? TURN_LEFT
             :  MOVE_FORWARD;
    }
}
