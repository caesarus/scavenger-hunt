package simulation.core;

import simulation.models.Simulation;

import java.util.List;

public interface ConverterActions
{
    Simulation deserialize(List<String> lines);
    List<String> serialize(Simulation simulation);
}
