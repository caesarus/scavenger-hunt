package simulation.core;

import java.io.File;
import java.util.List;

public interface FileActions
{
    List<String> readInput(File input);
    void writeOutput(File output, List<String> lines);
}
