package simulation;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import simulation.exceptions.ExitException;
import simulation.io.FileManager;
import simulation.models.Simulation;
import simulation.services.SimulationConverter;
import simulation.services.SimulationRunner;

import javax.annotation.Resource;
import java.io.File;
import java.util.List;

@Log4j2
@Component
public class Simulator
{
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(Config.class);

        Simulator simulator = context.getBean(Simulator.class);
        simulator.start(args);
    }

    @Resource private FileManager manager;
    @Resource private SimulationConverter converter;
    @Resource private SimulationRunner runner;

    private File input, output;
    private Simulation simulation;

    private void mayExit(Runnable action) {
        try {
            action.run();

        } catch (ExitException ex) {
            log.error(ex.getMessage());
            log.info("Simulation stopped");
            System.exit(1);
        }
    }

    private void start(String[] args) {
        log.info("Welcome to the peruvian scavenger hunt");
        this.validatePaths(args);
        this.loadSimulation();
        this.runner.run(this.simulation);
        this.writeResults();
        log.info("Simulation ended");
    }

    private void validatePaths(String[] args) {
        mayExit(() -> {
            this.manager.validateArgsCount(args);
            this.input = this.manager.validateInput(args[0]);
            this.output = this.manager.validateOutput(args[1]);
        });
    }

    private void loadSimulation() {
        mayExit(() -> {
            List<String> lines = this.manager.readInput(this.input);
            this.simulation = this.converter.deserialize(lines);
        });
    }

    private void writeResults() {
        mayExit(() -> {
            List<String> lines = this.converter.serialize(this.simulation);
            this.manager.writeOutput(this.output, lines);
        });
    }
}
