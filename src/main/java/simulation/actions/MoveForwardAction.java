package simulation.actions;

import simulation.enums.Direction;
import simulation.models.Area;
import simulation.models.Hunter;
import simulation.models.Position;

import java.util.List;
import java.util.Optional;

import static simulation.enums.Direction.*;

public class MoveForwardAction extends HunterAction
{
    private Area area;
    private List<Hunter> hunters;

    public MoveForwardAction(Hunter hunter, Area area, List<Hunter> hunters) {
        super(hunter);
        this.area = area;
        this.hunters = hunters;
    }

    @Override
    public void run() {
        Position next = this.nextPosition();
        this.info(
                "'%s' want to move forward @ (%d,%d)",
                this.hunter.getName(), next.getX(), next.getY()
        );

        if (area.isOutside(next)) {
            this.warn("'%s' can't leave the area !", this.hunter.getName());
            return;
        }

        if (area.isMountain(next)) {
            this.warn("'%s' want to climb a mountain !", this.hunter.getName());
            return;
        }

        Optional<String> current = hunters.stream()
                .filter(man -> !hunter.equals(man))
                .filter(hunter -> hunter.occupe(next))
                .map(Hunter::getName)
                .findFirst();
        if (current.isPresent()) {
            this.warn(
                    "'%s' want to be with '%s' @ (%d,%d) ",
                    this.hunter.getName(), current.get(), next.getX(), next.getY()
            );
            return;
        }

        this.debug("'%s' move @ (%d,%d)", this.hunter.getName(), next.getX(), next.getY());
        hunter.move(next);
    }

    Position nextPosition() {
        Direction side = hunter.getSide();
        Position step = (side == NORTH) ? new Position(0,-1)
                      : (side == SOUTH) ? new Position(0, 1)
                      : (side == WEST) ? new Position(-1,0)
                      : new Position(1,0);

        return Position.add(hunter.getPosition(), step);
    }
}
