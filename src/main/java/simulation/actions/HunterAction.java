package simulation.actions;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import simulation.core.HunterActions;
import simulation.models.Hunter;

@Log4j2
@AllArgsConstructor
public abstract class HunterAction implements HunterActions
{
    protected Hunter hunter;

    protected void debug(String pattern, Object... arguments) {
        log.info(String.format(pattern, arguments));
    }

    protected void info(String pattern, Object... arguments) {
        log.info(String.format(pattern, arguments));
    }

    protected void warn(String pattern, Object... arguments) {
        log.warn(String.format(pattern, arguments));
    }
}
