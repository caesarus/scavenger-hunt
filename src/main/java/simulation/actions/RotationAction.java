package simulation.actions;

import simulation.enums.Direction;
import simulation.models.Hunter;

public abstract class RotationAction extends HunterAction
{
    public RotationAction(Hunter hunter) {
        super(hunter);
    }

    public abstract Direction rotate(Direction current);

    @Override
    public void run() {
        Direction next = this.rotate(hunter.getSide());
        hunter.setSide(next);
        this.debug(
                "'%s' face to %s @ (%d,%d)", hunter.getName(), hunter.getSide(),
                hunter.getPosition().getX(), hunter.getPosition().getY()
        );
    }
}
