package simulation.actions;

import simulation.enums.Direction;
import simulation.models.Hunter;

import static simulation.enums.Direction.*;

public class TurnLeftAction extends RotationAction
{
    public TurnLeftAction(Hunter hunter) {
        super(hunter);
    }

    @Override
    public Direction rotate(Direction current) {
        this.info("'%s' rotate from %s to the left", this.hunter.getName(), current);
        return current == NORTH ? WEST
             : current == WEST ? SOUTH
             : current == SOUTH ? EAST
             : NORTH;
    }
}
