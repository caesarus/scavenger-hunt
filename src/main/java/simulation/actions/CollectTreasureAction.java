package simulation.actions;

import lombok.extern.log4j.Log4j2;
import simulation.models.Area;
import simulation.models.Hunter;
import simulation.models.Position;

@Log4j2
public class CollectTreasureAction extends HunterAction
{
    private Area area;

    public CollectTreasureAction(Hunter hunter, Area area) {
        super(hunter);
        this.area = area;
    }

    @Override
    public void run() {
        Position current = hunter.getPosition();
        this.info(
                "'%s' want to collect a treasure @ (%d,%d)",
                this.hunter.getName(), current.getX(), current.getY()
        );

        if (!hunter.canCollect()) {
            this.warn(
                    "'%s' can't collect an other one @ (%d,%d)",
                    this.hunter.getName(), current.getX(), current.getY()
            );
            return;
        }

        if (!area.collectTreasure(current)) {
            this.warn(
                    "'%s' can't collect one treasure @ (%d,%d)",
                    this.hunter.getName(), current.getX(), current.getY()
            );
            return;
        }

        this.debug(
                "'%s' collect one treasure @ (%d,%d)",
                this.hunter.getName(), current.getX(), current.getY()
        );
        hunter.collectTreasure();
    }
}
