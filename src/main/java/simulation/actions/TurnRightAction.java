package simulation.actions;

import simulation.enums.Direction;
import simulation.models.Hunter;

import static simulation.enums.Direction.*;

public class TurnRightAction extends RotationAction
{
    public TurnRightAction(Hunter hunter) {
        super(hunter);
    }

    @Override
    public Direction rotate(Direction current) {
        this.info("'%s' rotate from %s to the right", this.hunter.getName(), current);
        return current == NORTH ? EAST
             : current == EAST ? SOUTH
             : current == SOUTH ? WEST
             : NORTH;
    }
}
