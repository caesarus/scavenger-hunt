package simulation.io;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import simulation.core.FileActions;
import simulation.exceptions.ExitException;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

@Log4j2
@Service
public class FileManager implements FileActions
{
    public void validateArgsCount(String[] args) {
        if (args.length < 2) {
            throw new ExitException("missing arguments");
        }
    }

    public File validateInput(String path) {
        File input = new File(path);

        if (!input.exists()) throw new ExitException("input path ["+ path +"] doesn't exist");
        if (input.isDirectory()) throw new ExitException("input path ["+ path +"] is a folder");

        log.debug("Input file : "+ path);
        return input;
    }

    public File validateOutput(String path) {
        File output = new File(path);

        if (output.exists()) throw new ExitException("output path ["+ path +"] already exists");

        log.debug("Output file: "+ path);
        return output;
    }

    @Override
    public List<String> readInput(File input) {
        try {
            return FileUtils.readLines(input, Charset.defaultCharset());
        } catch (IOException ex) {
            log.error(ex.getMessage());
            throw new ExitException("unable to read input file");
        }
    }

    @Override
    public void writeOutput(File output, List<String> lines) {
        try {
            FileUtils.writeLines(output, lines);
        } catch (IOException ex) {
            log.error(ex.getMessage());
            throw new ExitException("unable to write output file");
        }
    }
}
