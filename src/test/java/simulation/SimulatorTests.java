package simulation;

import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import javax.annotation.Resource;

@Log4j2
public class SimulatorTests
{
    @Resource
    private String hello;

    @Test
    public void should_load_beans() {
        log.info(hello);
    }
}
