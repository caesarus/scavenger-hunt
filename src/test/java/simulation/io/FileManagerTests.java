package simulation.io;

import org.apache.commons.io.FileUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import simulation.BaseTests;
import simulation.exceptions.ExitException;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

public class FileManagerTests extends BaseTests
{
    @Rule
    public TemporaryFolder temp = new TemporaryFolder();

    @Resource
    private FileManager manager;

    @Test
    public void should_validate_args_count() {
        String[] args = {"fake", "args"};

        manager.validateArgsCount(args);
    }

    @Test(expected = ExitException.class)
    public void fail_validate_args_count_with_missing_args() {
        String[] args = {"fake"};

        manager.validateArgsCount(args);
    }

    @Test
    public void should_validate_input() throws IOException {
        File input = temp.newFile("input.txt");

        manager.validateInput(input.getPath());
    }

    @Test(expected = ExitException.class)
    public void fail_validate_missing_input() {
        File input = new File(temp.getRoot(),"input.txt");

        manager.validateInput(input.getPath());
    }

    @Test(expected = ExitException.class)
    public void fail_validate_input_folder() throws IOException {
        File input = temp.newFolder("input");

        manager.validateInput(input.getPath());
    }

    @Test
    public void validate_missing_output() {
        File output = new File(temp.getRoot(),"output.txt");

        manager.validateOutput(output.getPath());
    }

    @Test(expected = ExitException.class)
    public void fail_validate_present_output() throws IOException {
        File output = temp.newFile("output.txt");

        manager.validateOutput(output.getPath());
    }

    @Test
    public void should_read_input() throws IOException {
        List<String> raws = asList( "hello world", "foo bar baz");

        File input = temp.newFile("input.txt");
        FileUtils.writeLines(input, raws);

        List<String> reads = manager.readInput(input);
        assertThat(reads.size(), is(2));
        assertThat(reads, hasItem("hello world"));
    }

    @Test(expected = ExitException.class)
    public void fail_read_input_with_IOException_thrown() {
        File input = new File(temp.getRoot(), "input.txt");

        List<String> reads = manager.readInput(input);
        assertThat(reads.size(), is(0));
    }

    @Test
    public void should_write_output() {
        File output = new File(temp.getRoot(), "output.txt");

        manager.writeOutput(output, singletonList("fake content"));
        assertTrue(output.exists());
    }

    @Test(expected = ExitException.class)
    public void should_write_output_with_IOException_thrown() throws IOException {
        File output = temp.newFile("output.txt");
        output.setReadOnly();

        manager.writeOutput(output, singletonList("fake content"));
        assertThat(output.length(), is(0L));
    }
}
