package simulation.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import simulation.BaseTests;
import simulation.actions.HunterAction;
import simulation.actions.MoveForwardAction;
import simulation.actions.TurnLeftAction;
import simulation.actions.TurnRightAction;
import simulation.enums.Action;
import simulation.models.Hunter;
import simulation.models.Position;
import simulation.models.Simulation;

import javax.annotation.Resource;
import java.util.Optional;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static simulation.enums.Action.*;

public class SimulationRunnerTests extends BaseTests
{
    @Resource
    private SimulationRunner runner;

    @Mock
    private Simulation simulation;
    private Position position;
    @Mock
    private Hunter hunter;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        position = new Position(1,1);
    }

    @Test
    public void should_run() {
        runner.run(simulation);

        verify(simulation, atLeastOnce()).getHunters();
    }

    @Test
    public void should_iterate() {
        doReturn(true).when(hunter).idle();
        doReturn(position).when(hunter).getPosition();
        doReturn(singletonList(hunter)).when(simulation).getHunters();

        runner.iterate(simulation);

        verify(hunter, times(1)).getPosition();
    }

    @Test
    public void should_iterate_with_collect() {
        doReturn(false).when(hunter).idle();
        doReturn(position).when(hunter).getPosition();
        doReturn("fake").when(hunter).getName();
        doReturn(false).when(hunter).canCollect();
        doReturn(Optional.of(TURN_LEFT)).when(hunter).nextAction();
        doReturn(singletonList(hunter)).when(simulation).getHunters();

        runner.iterate(simulation);

        verify(simulation, times(1)).getArea();
    }

    @Test
    public void should_resolve_turn_left() {
        this.checkActionClass(TURN_LEFT, TurnLeftAction.class);
    }

    @Test
    public void should_resolve_turn_right() {
        this.checkActionClass(TURN_RIGHT, TurnRightAction.class);
    }

    @Test
    public void should_resolve_move_forward() {
        this.checkActionClass(MOVE_FORWARD, MoveForwardAction.class);
    }

    private <T> void checkActionClass(Action action, Class<T> type) {
        doReturn(Optional.of(action)).when(hunter).nextAction();

        Optional<HunterAction> resolved = runner.resolveAction(hunter, simulation);

        assertTrue(resolved.isPresent());
        assertTrue(type.equals(resolved.get().getClass()));
    }
}
