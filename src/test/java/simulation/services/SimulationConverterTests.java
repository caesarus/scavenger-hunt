package simulation.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.stereotype.Component;
import simulation.BaseTests;
import simulation.enums.Action;
import simulation.models.Area;
import simulation.models.Hunter;
import simulation.models.Position;
import simulation.models.Simulation;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.verify;
import static simulation.enums.Direction.SOUTH;
import static simulation.services.SimulationConverter.REGEX_AREA;

@Component
public class SimulationConverterTests extends BaseTests
{
    @InjectMocks
    private SimulationConverter converter;

    @Mock
    private SimulationFactory factory;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
    }

    // input

    @Test
    public void should_deserialize() {
        converter.deserialize(asList(
            "C - 5 - 6",
            "T - 1 - 2 - 3",
            "A - Lara - 0 - 1 - N - AADAG"
        ));

        ArgumentCaptor<List<Area>> argument1 = ArgumentCaptor.forClass(List.class);
        ArgumentCaptor<List<SimpleEntry<Position, Integer>>> argument2 = ArgumentCaptor.forClass(List.class);
        verify(factory).buildSimulation(argument1.capture(), anyList(), argument2.capture(), anyList());

        List<Area> areas = argument1.getValue();
        assertThat(areas.size(), is(1));

        List<SimpleEntry<Position, Integer>> treasures = argument2.getValue();
        assertThat(treasures.size(), is(1));
        assertThat(treasures.get(0).getValue(), is(3));
    }

    @Test
    public void should_deserialize_with_comment_line() {
        converter.deserialize(asList(
            "C - 4 - 6",
            "# comment line"
        ));

        ArgumentCaptor<List<Area>> argument = ArgumentCaptor.forClass(List.class);
        verify(factory).buildSimulation(argument.capture(), anyList(), anyList(), anyList());

        List<Area> areas = argument.getValue();
        assertThat(areas.size(), is(1));
    }

    @Test
    public void should_deserialize_without_area() {
        converter.deserialize(singletonList( "C - hello - 6" ));

        ArgumentCaptor<List<Area>> argument = ArgumentCaptor.forClass(List.class);
        verify(factory).buildSimulation(argument.capture(), anyList(), anyList(), anyList());

        List<Area> areas = argument.getValue();
        assertThat(areas.size(), is(0));
    }

    @Test
    public void should_match() {
        List<Object> items = new ArrayList<>();
        boolean matched = converter.match("C - 4 - 6", REGEX_AREA, converter::parseAreaLine, items::add);

        assertTrue(matched);
        assertThat(items.size(), is(1));
        assertTrue(items.get(0) instanceof Area);

        Area area = (Area) items.get(0);
        assertThat(area.getWidth(), is(4));
    }

    @Test
    public void should_not_match() {
        List<Object> items = new ArrayList<>();
        boolean matched = converter.match("fake line", REGEX_AREA, converter::parseAreaLine, items::add);

        assertFalse(matched);
        assertThat(items.size(), is(0));
    }

    @Test
    public void should_parse_area_line() {
        String[] groups = {"3", "4"};
        Area area = converter.parseAreaLine(groups);

        assertThat(area.getWidth(), is(3));
        assertThat(area.getHeight(), is(4));
    }

    @Test
    public void should_parse_mountain_line() {
        String[] groups = {"1", "2"};
        Position position = converter.parseMountainLine(groups);

        assertThat(position, notNullValue());
        assertThat(position.getX(), is(1));
        assertThat(position.getY(), is(2));
    }

    @Test
    public void should_parse_treasure_line() {
        String[] groups = {"1", "2", "3"};
        SimpleEntry<Position, Integer> entry = converter.parseTreasureLine(groups);

        Position position = entry.getKey();
        assertThat(position, notNullValue());
        assertThat(position.getX(), is(1));
        assertThat(entry.getValue(), is(3));
    }

    @Test
    public void should_parse_hunter_line() {
        String[] groups = {"Lara", "1", "2", "S", "AADADAGGA"};
        Hunter hunter = converter.parseHunterLine(groups);

        Position position = hunter.getPosition();
        assertThat(position, notNullValue());
        assertThat(position.getX(), is(1));
        assertThat(hunter.getName(), is("Lara"));
        assertThat(hunter.getSide(), is(SOUTH));
    }

    // output

    @Test
    public void should_serialize() {
        Area area = new Area(2,3);
        area.addMountain(new Position(0,0));
        area.addTreasure(new Position(0,1), 2);

        List<Hunter> hunters = singletonList(new Hunter("Lara", new Position(1,0), SOUTH, Action.from("AAGAD")));

        Simulation simulation = new Simulation(area, hunters);
        List<String> lines = converter.serialize(simulation);

        assertThat(lines, notNullValue());
        assertThat(lines.size(), is(4));
        assertThat(lines, hasItem("C - 2 - 3"));
        assertThat(lines, hasItem("M - 0 - 0"));
        assertThat(lines, hasItem("T - 0 - 1 - 2"));
        assertThat(lines, hasItem("A - Lara - 1 - 0 - S - 0"));
    }
}