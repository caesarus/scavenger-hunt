package simulation.services;

import org.junit.Before;
import org.junit.Test;
import simulation.BaseTests;
import simulation.enums.Action;
import simulation.exceptions.ExitException;
import simulation.models.*;

import javax.annotation.Resource;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;
import static simulation.enums.Direction.NORTH;

public class SimulationFactoryTests extends BaseTests
{
    @Resource
    private SimulationFactory factory;

    private Area area;
    private List<Position> mountains;
    private List<SimpleEntry<Position, Integer>> treasures;
    private List<Hunter> hunters;

    @Before
    public void createItems() {
        area = new Area(3,4);
        mountains = asList(
                new Position(10, 0),
                new Position(0, 0)
        );
        treasures = new ArrayList<>(asList(
                new SimpleEntry<>(new Position(10, 10), 1),
                new SimpleEntry<>(new Position(0, 0), 2),
                new SimpleEntry<>(new Position(0, 1), 3),
                new SimpleEntry<>(new Position(0, 1), 4)
        ));
        hunters = new ArrayList<>(asList(
                new Hunter("FAKE", new Position(10, 10), NORTH, Action.from("")),
                new Hunter("TOTO", new Position(0, 0), NORTH, Action.from("")),
                new Hunter("Lara", new Position(0, 1), NORTH, Action.from(""))
        ));
    }

    @Test
    public void should_build_simulation() {
        Simulation simulation = factory.buildSimulation(singletonList(area), mountains, treasures, hunters);

        assertThat(simulation, notNullValue());
        assertThat(simulation.getArea(), is (area));
        assertThat(simulation.getHunters().size(), is (1));
    }

    @Test
    public void prepare_area() {
        Area collected = factory.prepareArea(asList( area, area ));

        assertThat(collected, notNullValue());
    }

    @Test(expected = ExitException.class)
    public void fail_prepare_area_without_area() {
        factory.prepareArea(emptyList());
    }

    @Test
    public void should_prepare_mountains() {
        Position zero = new Position(0,0);
        area.addMountain(zero);

        factory.prepareMountains(area, mountains);

        assertTrue(area.isMountain(zero));
    }

    @Test
    public void should_prepare_treasures() {
        Position zero = new Position(0,0);
        area.addMountain(zero);

        factory.prepareTreasures(area, treasures);

        assertTrue(area.isMountain(zero));
        Position pos = new Position(0,1);
        assertFalse(area.isMountain(pos));
        Treasure treasure = (Treasure) area.getSquares().get(pos);
        assertThat(treasure.getQuantity(), is(3));

    }

    @Test
    public void should_prepare_hunters() {
        area.addMountain(new Position(0,0));

        List<Hunter> men = factory.prepareHunters(area, hunters);

        assertThat(men, notNullValue());
        assertThat(men.size(), is(1));
    }
}