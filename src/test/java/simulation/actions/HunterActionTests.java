package simulation.actions;

import org.junit.Before;
import simulation.BaseTests;
import simulation.enums.Action;
import simulation.enums.Direction;
import simulation.models.Hunter;
import simulation.models.Position;

public abstract class HunterActionTests extends BaseTests
{
    protected Hunter hunter;
    protected Position positionZero;

    @Before
    public void before() {
        positionZero = new Position(0,0);
        hunter = new Hunter("Lara", positionZero, Direction.NORTH, Action.from("AADAG"));
    }
}
