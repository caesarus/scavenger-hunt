package simulation.actions;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import simulation.models.Area;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

public class CollectTreasureActionTests extends HunterActionTests
{
    @Mock
    private Area area;

    private CollectTreasureAction action;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        super.before();
        action = new CollectTreasureAction(hunter, area);
    }

    @Test
    public void should_run() {
        doReturn(true).when(area).collectTreasure(any());

        action.run();

        assertThat(hunter.getTreasures(), is(1));
    }

    @Test
    public void should_run_when_hunter_can_not_collect() {
        hunter.collectTreasure();

        action.run();

        assertThat(hunter.getTreasures(), is(1));
    }

    @Test
    public void should_run_when_treasure_can_not_be_collected() {
        doReturn(false).when(area).collectTreasure(any());

        action.run();

        assertThat(hunter.getTreasures(), is(0));
    }
}