package simulation.actions;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static simulation.enums.Direction.WEST;

public class TurnLeftActionTests extends HunterActionTests
{
    @Test
    public void should_run() {
        HunterAction turnLeft = new TurnLeftAction(hunter);
        turnLeft.run();

        assertThat(hunter.getSide(), is(WEST));
    }
}