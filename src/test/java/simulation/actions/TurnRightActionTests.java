package simulation.actions;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static simulation.enums.Direction.EAST;

public class TurnRightActionTests extends HunterActionTests
{
    @Test
    public void should_run() {
        HunterAction turnRight = new TurnRightAction(hunter);
        turnRight.run();

        assertThat(hunter.getSide(), is(EAST));
    }
}