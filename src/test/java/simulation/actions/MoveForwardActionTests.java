package simulation.actions;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import simulation.enums.Action;
import simulation.models.Area;
import simulation.models.Hunter;
import simulation.models.Position;

import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static simulation.enums.Direction.NORTH;

public class MoveForwardActionTests extends HunterActionTests
{
    @Mock
    private Area area;
    @Mock
    private List<Hunter> hunters;
    private Hunter occupier;

    private MoveForwardAction action;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        super.before();

        occupier = new Hunter("Fake", positionZero, NORTH, Action.from("AA"));
        action = new MoveForwardAction(hunter, area, hunters);
    }

    @Test
    public void should_run() {
        doReturn(false).when(area).isOutside(any());
        doReturn(false).when(area).isMountain(any());
        doReturn(Stream.empty()).when(hunters).stream();

        action.run();

        assertThat(hunter.getPosition(), is(new Position(0,-1)));
    }

    @Test
    public void should_run_with_next_position_outside() {
        doReturn(true).when(area).isOutside(any());

        action.run();

        assertThat(hunter.getPosition(), is(positionZero));
    }

    @Test
    public void should_run_with_next_position_is_mountain() {
        doReturn(false).when(area).isOutside(any());
        doReturn(true).when(area).isMountain(any());

        action.run();

        assertThat(hunter.getPosition(), is(positionZero));
    }

    @Test
    public void should_run_with_next_position_occupied() {
        doReturn(false).when(area).isOutside(any());
        doReturn(false).when(area).isMountain(any());
        doReturn(Stream.of(occupier)).when(hunters).stream();

        action.run();

        assertThat(hunter.getPosition(), equalTo(new Position(0,-1)));
    }

    @Test
    public void should_next_position() {
        Position next = action.nextPosition();

        assertThat(next, equalTo(new Position(0,-1)));
    }
}