# Scavenger hunt

This project was built up with Java, Maven and Spring.
This project was designed around some core features.
It includes many design patterns such as Factory, Method pattern.


### Additional features

The input file must contains at least one Area flagged by 'C'.

The simulator rejects items when any position conflicts were detected, between items of the same nature or not.
Items were included according the following order : Area (C) > Mountain (M) > Treasure (T) > Hunter (A)

A hunter can idle during the whole simulation by written its line as following :
```ini
A - 1 - 2 - S - <without-sequence>
```
Please note the space between the last hyphen and the end.


### Usage

To launch the simulator, please open a shell and type:
```ini
java.exe -jar scavenger-hunt-standalone.jar <input-file> <output-file>
```


### Attachments
The subject and a test file are presents in the 'docs' folder.